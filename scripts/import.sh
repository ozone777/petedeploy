#!/bin/bash

while getopts n:p:r:m:z:f:u:b:c:d:e:f:g:h:i:a:v:w:s:x:y:l: option 
do 
case "${option}" 
	in
	n) project_name=${OPTARG};;
	p) db_root_pass=${OPTARG};; 
	r) route=${OPTARG};; 
	m) mysql_bin=${OPTARG};;
	z) file_name=${OPTARG};;
	u) project_url=${OPTARG};; 
	b) wpkey1=${OPTARG};;
	c) wpkey2=${OPTARG};;
	d) wpkey3=${OPTARG};;
	e) wpkey4=${OPTARG};;
	f) wpkey5=${OPTARG};;
	g) wpkey6=${OPTARG};;
	h) wpkey7=${OPTARG};;
	i) wpkey8=${OPTARG};;
	a) apache_conf=${OPTARG};;
	x) database_name=${OPTARG};;
	y) database_user=${OPTARG};;
	l) password_user=${OPTARG};;
	v) os=${OPTARG};;
	w) app_name=${OPTARG};;
	s) id=${OPTARG};;
esac 
done 

echo $project_name
echo $db_root_pass
echo $route
echo $mysql_bin
echo $file_name
echo $project_url
echo $wpkey1
echo $wpkey2
echo $wpkey3
echo $wpkey4
echo $wpkey5
echo $wpkey6
echo $wpkey7
echo $wpkey8
echo $apache_conf
echo $database_name
echo $database_user
echo $password_user
echo $os
echo $app_name
echo $id

cd $route

rm -rf $route/massive_file
rm -rf query.sql
rm -rf config.massive
rm -rf massive_file

gunzip -c $route/Pete/public/uploads/$file_name | tar xopf -

mv massive_file/filem $project_name
mv massive_file/query.sql query.sql
mv massive_file/config.massive config.massive

line=`grep "^domain" config.massive`
past_url="${line// /}"
past_url=${past_url#*"'"}
past_url=${past_url%"'"}

echo $past_url

prefix=`grep "^prefix" config.massive`
prefix="${prefix// /}"
prefix=${prefix#*"'"}
prefix=${prefix%"'"}

if [ $app_name == 'Wordpress' ]; then

echo "
define('WP_HOME','http://$project_url');
define('WP_SITEURL','http://$project_url');

define('DB_NAME', '$database_name');
define('DB_USER', '$database_user');
define('DB_PASSWORD', '$password_user');


define('AUTH_KEY',         '$wpkey1');
define('SECURE_AUTH_KEY',  '$wpkey2');
define('LOGGED_IN_KEY',    '$wpkey3');
define('NONCE_KEY',        '$wpkey4');
define('AUTH_SALT',        '$wpkey5');
define('SECURE_AUTH_SALT', '$wpkey6');
define('LOGGED_IN_SALT',   '$wpkey7');
define('NONCE_SALT',       '$wpkey8');
\$table_prefix  = '$prefix';

 " > otemp.txt

rm -rf $project_name/wp-config.php 
sed '21r otemp.txt' < $route/Pete/templates/wp-template-without-prefix.php > $project_name/wp-config.php 
#rm -rf otemp.txt

#Rename urls in database
sed -e "s/$past_url/$project_url/g" query.sql > query_new.sql
mv query_new.sql query.sql

#Security .htaccess best practices
rm -rf $route/$project_name/.htaccess
cp $route/Pete/templates/htaccess_main.txt $route/$project_name/.htaccess
cp $route/Pete/templates/htaccess_content.txt $route/$project_name/wp-content/uploads/.htaccess

#Security files permissions
find $project_name -type d -exec chmod 755 {} +
find $project_name -type f -exec chmod 644 {} +
chmod 600 $project_name/wp-config.php

fi

if [ $app_name == 'Drupal' ]; then

echo "

\$databases = array (
  'default' => 
  array (
    'default' => 
    array (
      'database' => '$database_name',
      'username' => '$database_user',
      'password' => '$password_user',
      'host' => 'localhost',
      'port' => '',
      'driver' => 'mysql',
      'prefix' => '',
    ),
  ),
);

" > otemp.txt

rm -rf $project_name/sites/default/settings.php
sed '215r otemp.txt' < $route/Pete/templates/dp-template.php > $project_name/sites/default/settings.php
	
fi


#MAC
if [ $os == 'osx' ]; then
   osascript -e "do shell script \"echo '127.0.0.1 $project_url' >> /private/etc/hosts\" with administrator privileges"

echo "
#---$id
<VirtualHost *:80>
    ServerName $project_url
	ServerAlias www.$project_url
    DocumentRoot '$route/$project_name'
    ErrorLog 'logs/demo_log'
</VirtualHost>
" >> $apache_conf

#sed -i '' "s/$past_url/$project_url/g" query.sql



fi

#Linux
if [ $os == 'linux' ]; then
	echo "
	<VirtualHost *:80>

	    ServerName $project_url
	    ServerAlias www.$project_url
	    DocumentRoot $route/$project_name
	    ErrorLog /var/www/$project_name/error.log
	    CustomLog /var/www/$project_name/requests.log combined

	      <Directory $route/$project_name>
	            AllowOverride All
	        </Directory>

	</VirtualHost>" > $apache_conf/$project_name.conf
	
fi


#Mysql Commands
$mysql_bin --host=localhost -uroot -p$db_root_pass -e "create database $database_name"
$mysql_bin --host=localhost -uroot -p$db_root_pass -e "CREATE USER $database_user@localhost"
$mysql_bin --host=localhost -uroot -p$db_root_pass -e "SET PASSWORD FOR $database_user@localhost = PASSWORD('$password_user')"
$mysql_bin --host=localhost -uroot -p$db_root_pass -e "GRANT ALL PRIVILEGES ON $database_name.* TO $database_user@localhost IDENTIFIED BY '$password_user'"
$mysql_bin --host=localhost -uroot -p$db_root_pass $database_name < query.sql



