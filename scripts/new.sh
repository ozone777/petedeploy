#!/bin/bash

while getopts n:u:p:r:m:a:x:y:z:b:c:d:e:f:g:h:i:v:w:s: option 
do 
case "${option}" 
	in 
	v) os=${OPTARG};;
	n) project_name=${OPTARG};; 
	u) project_url=${OPTARG};; 
	p) db_root_pass=${OPTARG};; 
	r) route=${OPTARG};; 
	m) mysql_bin=${OPTARG};;
	a) apache_conf=${OPTARG};;
	x) database_name=${OPTARG};;
	y) database_user=${OPTARG};;
	z) password_user=${OPTARG};;
	w) app_name=${OPTARG};;
	b) wpkey1=${OPTARG};;
	c) wpkey2=${OPTARG};;
	d) wpkey3=${OPTARG};;
	e) wpkey4=${OPTARG};;
	f) wpkey5=${OPTARG};;
	g) wpkey6=${OPTARG};;
	h) wpkey7=${OPTARG};;
	i) wpkey8=${OPTARG};;
	s) id=${OPTARG};;
esac 
done 


echo "os: "$os
echo "project_name: "$project_name
echo "project_url: "$project_url
echo "db_root_pass: "$db_root_pass
echo "route: "$route
echo "mysql_bin :"$mysql_bin
echo "apache_conf: "$apache_conf
echo "database: "$database_name
echo "user: "$database_user
echo "password: "$password_user
echo "app_name: "$app_name
echo "wpkey1: "$wpkey1
echo "wpkey2: "$wpkey2
echo "wpkey3: "$wpkey3
echo "wpkey4: "$wpkey4
echo "wpkey5: "$wpkey5
echo "wpkey6: "$wpkey6
echo "wpkey7: "$wpkey7
echo "wpkey8: "$wpkey8
echo "id: "$id

cd $route

if [ $app_name == 'Wordpress' ]; then

rm -rf wordpress.zip
cp $route/Pete/templates/wordpress.zip wordpress.zip
unzip wordpress.zip
mv wordpress $project_name

echo "
define('WP_HOME','http://$project_url');
define('WP_SITEURL','http://$project_url');

define('DB_NAME', '$database_name');
define('DB_USER', '$database_user');
define('DB_PASSWORD', '$password_user');


define('AUTH_KEY',         '$wpkey1');
define('SECURE_AUTH_KEY',  '$wpkey2');
define('LOGGED_IN_KEY',    '$wpkey3');
define('NONCE_KEY',        '$wpkey4');
define('AUTH_SALT',        '$wpkey5');
define('SECURE_AUTH_SALT', '$wpkey6');
define('LOGGED_IN_SALT',   '$wpkey7');
define('NONCE_SALT',       '$wpkey8');

 " > otemp.txt
 
sed '21r otemp.txt' < $route/Pete/templates/wp-template.php > $project_name/wp-config.php 

#Incrementar seguridad .ssh
#Security .htaccess best practices
rm -rf $route/$project_name/.htaccess
cp $route/Pete/templates/htaccess_main.txt $route/$project_name/.htaccess
cp $route/Pete/templates/htaccess_content.txt $route/$project_name/wp-content/uploads/.htaccess

#Security files permissions
find $project_name -type d -exec chmod 755 {} +
find $project_name -type f -exec chmod 644 {} +
chmod 600 $project_name/wp-config.php

fi

if [ $app_name == 'Drupal' ]; then

rm -rf drupal-7.14.zip	
cp $route/Pete/templates/drupal-7.41.zip drupal-7.41.zip
unzip drupal-7.41.zip	
mv drupal-7.41 $project_name

fi

#For MAC
if [ $os == 'osx' ]; then
   osascript -e "do shell script \"echo '127.0.0.1 $project_url' >> /private/etc/hosts\" with administrator privileges"

echo "
#---$id
<VirtualHost *:80>
    ServerName $project_url
	ServerAlias www.$project_url
    DocumentRoot '$route/$project_name'
    ErrorLog 'logs/demo_log'
</VirtualHost>

" >> $apache_conf

fi

#For Linux
if [ $os == 'linux' ]; then
	echo "
	<VirtualHost *:80>

	    ServerName $project_url
	    ServerAlias www.$project_url
	    DocumentRoot $route/$project_name
	    ErrorLog /var/www/$project_name/error.log
	    CustomLog /var/www/$project_name/requests.log combined

	      <Directory $route/$project_name>
	            AllowOverride All
	        </Directory>

	</VirtualHost>" > $apache_conf/$project_name.conf
	
fi

#mysql Commands
echo $mysql_bin --host=localhost -uroot -p$db_root_pass -e "create database $database_name"
echo $mysql_bin --host=localhost -uroot -p$db_root_pass -e "CREATE USER $database_user@localhost"
echo $mysql_bin --host=localhost -uroot -p$db_root_pass -e "SET PASSWORD FOR $database_user@localhost = PASSWORD('$password_user')"
echo $mysql_bin --host=localhost -uroot -p$db_root_pass -e "GRANT ALL PRIVILEGES ON $database_name.* TO $database_user@localhost IDENTIFIED BY '$password_user'"

$mysql_bin --host=localhost -uroot -p$db_root_pass -e "create database $database_name"
$mysql_bin --host=localhost -uroot -p$db_root_pass -e "CREATE USER $database_user@localhost"
$mysql_bin --host=localhost -uroot -p$db_root_pass -e "SET PASSWORD FOR $database_user@localhost = PASSWORD('$password_user')"
$mysql_bin --host=localhost -uroot -p$db_root_pass -e "GRANT ALL PRIVILEGES ON $database_name.* TO $database_user@localhost IDENTIFIED BY '$password_user'"







