#!/bin/bash

while getopts b: option 
do 
case "${option}" 
	in 
	b) bin=${OPTARG};; 
esac 
done 

runcommand='httpd -v'
versiondescription=`$bin$runcommand`
echo $versiondescription > description.txt
version=`grep "^Server version:" description.txt`
version="${version// /}"
version=${version#*/}
version=${version%(*}
echo $version