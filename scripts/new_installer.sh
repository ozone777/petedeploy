#curl -o new_installer.sh -L https://www.dropbox.com/s/gupu371mdudly84/new_installer.sh && sh new_installer.sh

if [ -f /etc/debian_version ]; then
OS="Debian"
VER=$(cat /etc/debian_version)
 
elif [ -f /etc/redhat-release ]; then
OS="Red Hat"
VER=$(cat /etc/redhat-release)
echo "Centospete"
#objetivos

cd /tmp 
curl -sS https://getcomposer.org/installer | php  
mv composer.phar /usr/local/bin/composer
cd /var/www

#Git mode
git clone -b Pete --single-branch git://ozone.sourcerepo.com/ozone/Massive.git
mv Massive Pete

chmod -R 755 Pete
chown -R apache:apache Pete
cd /var/www/Pete
composer install
cp .env.example .env
php artisan key:generate
echo '' > database/database.sqlite
php artisan migrate
php artisan db:seed --class=MassiveSeeder
php artisan addfields --app_root=/var/www --server_conf=/etc/httpd/conf.d --os=linux --os_version=centos7
php artisan addhexkey
echo "Please insert the DB password for root user:"
read mysqlpass
php artisan addpass $mysqlpass
mkdir /var/www/Pete/public/uploads
mkdir /var/www/Pete/public/export
mkdir /var/www/Pete/public/trash

echo "

<VirtualHost *:80>

    ServerName localhost
    ServerAlias localhost
    DocumentRoot /var/www/Pete/public
    ErrorLog /var/www/Pete/error.log
    CustomLog /var/www/Pete/requests.log combined

      <Directory /var/www/Pete>
            AllowOverride All
        </Directory>

</VirtualHost>" > /etc/httpd/conf.d/pete.conf

cd
ln -s /var/www sites
ln -s /etc/httpd/conf.d configs
chown -R apache:apache /var/www
chown -R apache:apache /etc/httpd/conf.d
chmod -R 755 /var/www/Pete

#Suoers file for apache reload server
echo "apache ALL=NOPASSWD: /bin/systemctl reload httpd.service" >> /etc/sudoers
find /etc/sudoers -type f -exec sed -i 's/Defaults    requiretty/#Defaults    requiretty/g' {} \;
apachectl restart  

 
elif [ -f /etc/SuSE-release ]; then
OS="SuSE"
VER=$(cat /etc/SuSE-release)
 
else
OS=$(uname -s)
VER=$(uname -r)
fi 

if [ "${OS}" = "Darwin" ] ; then
	echo "Aca es para Mac"
	
	file="/Applications/MAMP"
	if [ -d "$file" ]
	then
		curl -sS https://getcomposer.org/installer | php
		mv composer.phar /usr/local/bin/composer
		curl -o httpd.conf -L https://www.dropbox.com/s/g255wp44a7n8e7p/httpd.conf
		mv httpd.conf /Applications/MAMP/conf/apache/httpd.conf
		
		#Git mode
		
		#Pete completo logica login
		#cd /Applications/MAMP/htdocs/ && git clone -b petedeploy git@ozone.sourcerepo.com:ozone/Massive.git 
		#Pete logica local simple sin login
		#cd /Applications/MAMP/htdocs/ && git clone -b petelocal git@ozone.sourcerepo.com:ozone/Massive.git 	
		
		#zip mode
		cd /Applications/MAMP/htdocs
		curl -o Petedeploy.zip -L https://www.dropbox.com/s/49kn944ppz2671x/Petedeploy.zip
		unzip Petedeploy.zip
		mv Petedeploy Pete
		cd /Applications/MAMP/htdocs/Pete
		composer install
		cp .env.example .env
		php artisan key:generate
		echo '' > database/database.sqlite
		php artisan migrate
		php artisan db:seed --class=MassiveSeeder
		php artisan addfields --app_root=/Applications/MAMP/htdocs --server_conf=/Applications/MAMP/conf/apache/extra/httpd-vhosts.conf --os=osx --os_version=yosemite --mysql_bin=/Applications/MAMP/Library/bin/
		php artisan addhexkey
		php artisan addpass root
		php artisan addtemplate t[i].wordpresspete.com
		mkdir /Applications/MAMP/htdocs/Pete/public/uploads
		mkdir /Applications/MAMP/htdocs/Pete/public/export
		mkdir /Applications/MAMP/htdocs/Pete/trash

		echo "
		<VirtualHost *:80>

		    ServerName local.wordpresspete.com
		    ServerAlias local.wordpresspete.com
		    DocumentRoot /Applications/MAMP/htdocs/Pete/public
		    ErrorLog /Applications/MAMP/htdocs/Pete/error.log
		    CustomLog /Applications/MAMP/htdocs/Pete/requests.log combined

		      <Directory /Applications/MAMP/htdocs/Pete>
		            AllowOverride All
		        </Directory>

		</VirtualHost>" >> /Applications/MAMP/conf/apache/extra/httpd-vhosts.conf

		osascript -e "do shell script \"echo '127.0.0.1 local.wordpresspete.com' >> /private/etc/hosts && sudo apachectl stop && /Applications/MAMP/bin/startApache.sh\" with administrator privileges"
		#osascript -e "do shell script \"/Applications/MAMP/bin/startApache.sh\" with administrator privileges"
		/Applications/MAMP/bin/startmySQL.sh > /dev/null 2>&1
		open http://local.wordpresspete.com
		
	else
		echo "Plesase install MAMP for Mac and then run this script again. https://www.mamp.info/en/downloads/"
	fi
	
fi

echo "Operating System Name: $OS"
echo "Operating System Version: $VER"
