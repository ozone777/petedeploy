#!/bin/bash

while getopts n:q:u:p:r:m:a:x:y:z:b:c:d:e:f:g:h:i:v:w:s:l: option 
do 
case "${option}" 
	in 
	q) mysqldump=${OPTARG};;
	n) project_name=${OPTARG};; 
	u) project_url=${OPTARG};; 
	p) db_root_pass=${OPTARG};; 
	r) route=${OPTARG};; 
	m) mysql_bin=${OPTARG};;
	a) apache_conf=${OPTARG};;
	x) database_name=${OPTARG};;
	y) database_user=${OPTARG};;
	z) password_user=${OPTARG};;
	b) wpkey1=${OPTARG};;
	c) wpkey2=${OPTARG};;
	d) wpkey3=${OPTARG};;
	e) wpkey4=${OPTARG};;
	f) wpkey5=${OPTARG};;
	g) wpkey6=${OPTARG};;
	h) wpkey7=${OPTARG};;
	i) wpkey8=${OPTARG};;
	v) os=${OPTARG};;
	w) app_name=${OPTARG};;
	s) id=${OPTARG};;
	l) past_url=${OPTARG};;
esac 
done 

#echo "project_name: "$project_name
#echo "project_url: "$project_url
#echo "db_root_pass: "$db_root_pass
#echo "route: "$route
#echo "mysql_bin: "$mysql_bin
#echo "mysqldump: "$mysqldump
#echo "apache_conf: "$apache_conf
#echo "database_name: "$database_name
#echo "database_user: "$database_user
#echo "password_user: "$password_user

cd $route

if [ $app_name == 'Wordpress' ]; then

#Get DB Name
odb=`grep "^define('DB_NAME'," $project_name/wp-config.php`
odb="${odb// /}"
odb=${odb#*,"'"}
odb=${odb%"'");}

#Get DB Prefix
prefix=`grep "table_prefix" $project_name/wp-config.php`
prefix="${prefix// /}"
prefix=${prefix#*="'"}
prefix=${prefix%"'";}

echo "
define('WP_HOME','http://$project_url');
define('WP_SITEURL','http://$project_url');

define('DB_NAME', '$database_name');
define('DB_USER', '$database_user');
define('DB_PASSWORD', '$password_user');


define('AUTH_KEY',         '$wpkey1');
define('SECURE_AUTH_KEY',  '$wpkey2');
define('LOGGED_IN_KEY',    '$wpkey3');
define('NONCE_KEY',        '$wpkey4');
define('AUTH_SALT',        '$wpkey5');
define('SECURE_AUTH_SALT', '$wpkey6');
define('LOGGED_IN_SALT',   '$wpkey7');
define('NONCE_SALT',       '$wpkey8');

\$table_prefix  = '$prefix';

 " > otemp.txt
 
rm -rf $project_name/wp-config.php 
sed '21r otemp.txt' < $route/Pete/templates/wp-template-without-prefix.php > $project_name/wp-config.php 

fi

if [ $app_name == 'Drupal' ]; then

#Get DB Name
odb=`grep "'database' =>" $project_name/sites/default/settings.php | tail -1`
odb="${odb// /}"
odb=${odb#*=>"'"}
odb=${odb%"'",}

echo "

\$databases = array (
  'default' => 
  array (
    'default' => 
    array (
      'database' => '$database_name',
      'username' => '$database_user',
      'password' => '$password_user',
      'host' => 'localhost',
      'port' => '',
      'driver' => 'mysql',
      'prefix' => '',
    ),
  ),
);

" > otemp.txt
 
rm -rf $project_name/sites/default/settings.php
sed '215r otemp.txt' < $route/Pete/templates/dp-template.php > $project_name/sites/default/settings.php

fi


#Server configuration in MAC
if [ $os == 'osx' ]; then

row=`awk "/#---$id/{ print NR; exit }" $apache_conf`

rowlimit=$((row+6))
cond=$row,$rowlimit
d=d

sed -i '' "$row,$rowlimit$d" $apache_conf	
	
osascript -e "do shell script \"echo '127.0.0.1 $project_url' >> /private/etc/hosts\" with administrator privileges"

echo "
#---$id
<VirtualHost *:80>
    ServerName $project_url
	ServerAlias www.$project_url
    DocumentRoot '$route/$project_name'
    ErrorLog 'logs/demo_log'
</VirtualHost>
" >> $apache_conf


fi

#Server configuration in Linux
if [ $os == 'linux' ]; then

rm -rf $apache_conf/$project_name

echo "
	<VirtualHost *:80>

	    ServerName $project_url
	    ServerAlias www.$project_url
	    DocumentRoot $route/$project_name
	    ErrorLog /var/www/$project_name/error.log
	    CustomLog /var/www/$project_name/requests.log combined

	      <Directory $route/$project_name>
	            AllowOverride All
	        </Directory>

	</VirtualHost>" > $apache_conf/$project_name.conf
	
fi

#Erase temporal file
rm -rf otemp.txt

#Export database
rm -rf query.sql
$mysqldump --host=localhost -uroot -p$db_root_pass $odb > query.sql

#Replace domain in database
sed -e "s/$past_url/$project_url/g" query.sql > query_new.sql
mv query_new.sql query.sql

#Crea base de datos paralela y usuario
$mysql_bin --host=localhost -uroot -p$db_root_pass -e "create database $database_name"
$mysql_bin --host=localhost -uroot -p$db_root_pass -e "CREATE USER $database_user@localhost"
$mysql_bin --host=localhost -uroot -p$db_root_pass -e "SET PASSWORD FOR $database_user@localhost = PASSWORD('$password_user')"
$mysql_bin --host=localhost -uroot -p$db_root_pass -e "GRANT ALL PRIVILEGES ON $database_name.* TO $database_user@localhost IDENTIFIED BY '$password_user'"

#Importa sql a la nueva base de datos
$mysql_bin --host=localhost -uroot -p$db_root_pass $database_name < query.sql


