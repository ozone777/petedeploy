#!/bin/bash

while getopts n:u:p:r:m:a:x:y:z:b:c:d:e:f:g:h:i:v:w: option 
do 
case "${option}" 
	in 
	v) os=${OPTARG};;
	w) server=${OPTARG};;
	n) project_name=${OPTARG};; 
	u) project_url=${OPTARG};; 
	p) db_root_pass=${OPTARG};; 
	r) route=${OPTARG};; 
	m) mysql_bin=${OPTARG};;
	a) apache_conf=${OPTARG};;
	x) database_name=${OPTARG};;
	y) database_user=${OPTARG};;
	z) password_user=${OPTARG};;
	b) wpkey1=${OPTARG};;
	c) wpkey2=${OPTARG};;
	d) wpkey3=${OPTARG};;
	e) wpkey4=${OPTARG};;
	f) wpkey5=${OPTARG};;
	g) wpkey6=${OPTARG};;
	h) wpkey7=${OPTARG};;
	i) wpkey8=${OPTARG};;
esac 
done 

echo $os
echo $server
echo $project_name
echo $project_url
echo $db_root_pass
echo $route
echo $mysql_bin
echo $apache_conf
cd $route

pwd
ls -l
rm -rf latest.zip
curl -O https://wordpress.org/latest.zip