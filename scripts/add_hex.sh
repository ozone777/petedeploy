#!/bin/bash

while getopts r:k: option 
do 
case "${option}" 
	in 
	r) route=${OPTARG};; 
	k) key=${OPTARG};; 
esac 
done 

cd $route
sed '/HEX_KEY/d' .env >.env_new
mv .env_new .env
echo "HEX_KEY=$key" >> .env