#!/bin/bash

while getopts r: option 
do 
case "${option}" 
	in 
	r) route=${OPTARG};; 
esac 
done 

prefix=`grep "^HEX_KEY" $route/Pete/.env`
prefix="${prefix// /}"
prefix=${prefix#*=}
echo $prefix