#!/bin/bash

while getopts v:n:u:r:a:s:x: option 
do 
case "${option}" 
	in 
	v) os=${OPTARG};;
	n) adomain_name=${OPTARG};; 
	u) site_url=${OPTARG};; 
	r) route=${OPTARG};; 
	a) apache_conf=${OPTARG};;
	s) id=${OPTARG};;
	x) filename=${OPTARG};;
esac 
done 

echo $os
echo $adomain_name
echo $site_url
echo $route
echo $apache_conf
echo $id
echo $filename

if [ $os == 'osx' ]; then
   osascript -e "do shell script \"echo '127.0.0.1 $adomain_name' >> /private/etc/hosts\" with administrator privileges"

echo "
#---r$id
<VirtualHost *:80>
        ServerName $adomain_name
        Redirect 301 / http://$site_url
</VirtualHost>
" >> $apache_conf

fi

#For Linux
if [ $os == 'linux' ]; then
	echo "
	<VirtualHost *:80>
	        ServerName $adomain_name
	        Redirect 301 / http://$site_url
	</VirtualHost>
	" > $apache_conf/$filename.conf
fi