#!/bin/bash

while getopts v:n:p:r:m:a:w:s: option 
do 
case "${option}" 
	in 
	v) os=${OPTARG};;
	n) project_name=${OPTARG};; 
	p) db_root_pass=${OPTARG};; 
	r) route=${OPTARG};; 
	m) mysql_bin=${OPTARG};;
	a) apache_conf=${OPTARG};;
	w) app_name=${OPTARG};;
	s) id=${OPTARG};;
esac 
done 

cd $route

echo $os
echo $project_name
echo $db_root_pass
echo $route
echo $mysql_bin
echo $apache_conf
echo $app_name
echo $id


if [ $app_name == 'Wordpress' ]; then

odb=`grep "^define('DB_NAME'," $project_name/wp-config.php`
odb="${odb// /}"
odb=${odb#*,"'"}
odb=${odb%"'");}

fi

if [ $app_name == 'Drupal' ]; then

odb=`grep "'database' =>" $project_name/sites/default/settings.php | tail -1`
odb="${odb// /}"
odb=${odb#*=>"'"}
odb=${odb%"'",}
	
fi

#si es MAC
if [ $os == 'osx' ]; then

row=`awk "/#---$id/{ print NR; exit }" $apache_conf`
echo 'awk "/#---$id/{ print NR; exit }" $apache_conf'
rowlimit=$((row+6))
cond=$row,$rowlimit
d=d
#echo "sed '$cond' regular.txt"
echo "sed -i '' '$row,$rowlimit$d' $apache_conf"
sed -i '' "$row,$rowlimit$d" $apache_conf


#$mysql_bin --host=localhost -uroot -p$db_root_pass -e "drop database $odb ;"

/Applications/MAMP/Library/bin/apachectl graceful
	
fi


if [ $os == 'linux' ]; then

#rm -rf $project_name
#$mysql_bin --host=localhost -uroot -p$db_root_pass -e "drop database $odb ;"
rm -rf $apache_conf/$project_name.conf
sudo /bin/systemctl reload httpd.service	

fi

mv $project_name Pete/trash/$project_name


