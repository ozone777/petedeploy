#!/bin/bash

while getopts r:k: option 
do 
case "${option}" 
	in 
	r) route=${OPTARG};; 
	k) key=${OPTARG};; 
esac 
done 

cd $route
sed '/DB_PASSWORD/d' .env >.env_new
mv .env_new .env
echo "DB_PASSWORD=$key" >> .env