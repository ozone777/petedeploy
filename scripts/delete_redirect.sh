#!/bin/bash

while getopts v:n:u:r:a:s:x: option 
do 
case "${option}" 
	in 
	v) os=${OPTARG};;
	n) adomain_name=${OPTARG};; 
	u) site_url=${OPTARG};; 
	r) route=${OPTARG};; 
	a) apache_conf=${OPTARG};;
	s) id=${OPTARG};;
	x) filename=${OPTARG};;
esac 
done 

#si es MAC
if [ $os == 'osx' ]; then

row=`awk "/#---r$id/{ print NR; exit }" $apache_conf`
rowlimit=$((row+4))
cond=$row,$rowlimit
d=d
sed -i '' "$row,$rowlimit$d" $apache_conf

fi


if [ $os == 'linux' ]; then

rm -rf $apache_conf/$filename.conf

fi
