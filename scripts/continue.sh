#!/bin/bash

while getopts v:n:p:r:m:a:w:s:u: option 
do 
case "${option}" 
	in 
	v) os=${OPTARG};;
	n) project_name=${OPTARG};; 
	p) db_root_pass=${OPTARG};; 
	r) route=${OPTARG};; 
	m) mysql_bin=${OPTARG};;
	a) apache_conf=${OPTARG};;
	w) app_name=${OPTARG};;
	s) id=${OPTARG};;
	u) project_url=${OPTARG};;
esac 
done 

cd $route

echo $os
echo $project_name
echo $db_root_pass
echo $route
echo $mysql_bin
echo $apache_conf
echo $app_name
echo $id


#si es MAC
if [ $os == 'osx' ]; then
	
row=`awk "/#---$id/{ print NR; exit }" $apache_conf`
echo 'awk "/#---$id/{ print NR; exit }" $apache_conf'
rowlimit=$((row+6))
cond=$row,$rowlimit
d=d
#echo "sed '$cond' regular.txt"
echo "sed -i '' '$row,$rowlimit$d' $apache_conf"
sed -i '' "$row,$rowlimit$d" $apache_conf

echo "
	#---$id
	<VirtualHost *:80>
	    ServerName $project_url
		ServerAlias www.$project_url
	    DocumentRoot '$route/$project_name'
	    ErrorLog 'logs/demo_log'
	</VirtualHost>" >> $apache_conf	
fi


if [ $os == 'linux' ]; then
	
rm -rf $apache_conf/$project_name.conf

echo "
<VirtualHost *:80>

	    ServerName $project_url
	    ServerAlias www.$project_url
	    DocumentRoot $route/$project_name
	    ErrorLog /var/www/$project_name/error.log
	    CustomLog /var/www/$project_name/requests.log combined

	      <Directory $route/$project_name>
	            AllowOverride All
	        </Directory>

</VirtualHost>" > $apache_conf/$project_name.conf	

fi



