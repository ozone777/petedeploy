<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use View;
use Route;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /*
		app('view')->composer('layouts.master', function($view)
		{
		        $action = app('request')->route()->getAction();

		        $controller = class_basename($action['controller']);

		        list($controller, $action) = explode('@', $controller);

		        $view->with(compact('controller', 'action'));
		});
		
		View::composer('*', function($view)
		    {
		        $view->with('current_user', Auth::user());
		    });
		*/
		
	   
		   
   	View::composer('*', function($view){
		
	  $current_user = Auth::user();
	  $controller = substr(class_basename(Route::currentRouteAction()), 0, (strpos(class_basename(Route::currentRouteAction()), '@') -0) );
	  $action = explode('@',Route::currentRouteAction())[1];
				
   	  $view->with(compact('current_user','controller','action'));
	  
    });
		
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
