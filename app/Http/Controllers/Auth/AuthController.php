<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Log;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
		$users = User::orderBy('id', 'desc');
		
		if($users->count() >= 1){
			$this->middleware('auth', ['except' => array('getLogin', 'postLogin','postRegister')]);
		}else{
			$this->middleware('guest', ['except' =>  'getLogout']);
		}
	
    }
	
	
    public function getLogin()
    {
        if (view()->exists('auth.authenticate')) {
            return view('auth.authenticate');
        }
		$prueba = "Estoy probando";
		$users = User::orderBy('id', 'desc');
		
		if($users->count() >= 1){
			$sw_user = "true";
		}else{
			$sw_user = "false";
		}
		
		#return view('sites.create',compact('actions','apps','output'));
		
        return view('auth.login',compact('sw_user'));
    }
	
    public function postRegister(Request $request)
    {
		#$users = User::orderBy('id', 'desc');
		
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        Auth::login($this->create($request->all()));

        return redirect($this->redirectPath());
	
    }
	
    

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
		if(User::count() == 0 ){
			
			Log::info('No tiene usuarios');
			
	        return User::create([
				'admin' => true,
	            'name' => $data['name'],
	            'email' => $data['email'],
	            'password' => bcrypt($data['password']),
	        ]);
			
		}else{
			
	        return User::create([
	            'name' => $data['name'],
	            'email' => $data['email'],
	            'password' => bcrypt($data['password']),
	        ]);
			
		}
		
        
    }
}
