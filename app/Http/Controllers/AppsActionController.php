<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\AppsAction;
use Illuminate\Http\Request;

class AppsActionController extends Controller {
	
	
	public function __construct()
	    {
	        $this->middleware('auth');
	    }
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$apps_actions = AppsAction::orderBy('id', 'desc')->paginate(10);

		return view('apps_actions.index', compact('apps_actions'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('apps_actions.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$apps_action = new AppsAction();

		$apps_action->app_id = $request->input("app_id");
        $apps_action->action_id = $request->input("action_id");

		$apps_action->save();

		return redirect()->route('apps_actions.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$apps_action = AppsAction::findOrFail($id);

		return view('apps_actions.show', compact('apps_action'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$apps_action = AppsAction::findOrFail($id);

		return view('apps_actions.edit', compact('apps_action'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$apps_action = AppsAction::findOrFail($id);

		$apps_action->app_id = $request->input("app_id");
        $apps_action->action_id = $request->input("action_id");
		$apps_action->script = $request->input("script");

		$apps_action->save();

		return redirect()->route('apps_actions.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$apps_action = AppsAction::findOrFail($id);
		$apps_action->delete();

		return redirect()->route('apps_actions.index')->with('message', 'Item deleted successfully.');
	}

}
