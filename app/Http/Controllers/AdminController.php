<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Site;
use App\User;
use Illuminate\Http\Request;
use Input;

class AdminController extends Controller {

	public function __construct()
	    {
	        #$this->middleware('csrf');
	        $this->middleware('auth');
			
	    }
	
	public function list_users(){
		
		$users = User::orderBy('id', 'desc')->paginate(10);		
	    return view('admin.list_users',compact('users'));
		
	}
	
   public function force_delete()
   {
	
	$site = Site::onlyTrashed()->findOrFail(Input::get('id'));
	
	$site->forceDelete();
	
	$sites = Site::onlyTrashed()->orderBy('id', 'desc')->paginate(10);
      
	return view('sites.trash', compact('sites'));
   }

	 public function edit_user()
	{
		
		$user = User::findOrFail(Input::get('id'));
       
		return view('admin.edit_user', compact('user'));
	}
	
	

}
