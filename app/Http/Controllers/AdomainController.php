<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Log;
use App\Adomain;
use Illuminate\Http\Request;
use App\User;
use Validator;

class AdomainController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$adomains = Adomain::orderBy('id', 'desc')->paginate(10);

		return view('adomains.index', compact('adomains'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('adomains.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		
		$adomain = new Adomain();
		
		$adomain->name = $request->input("name");
		$adomain->site_id = $request->input("site_id");
		
		
      $validator = Validator::make($request->all(), [
               'name' => 'required|unique:adomains',
      ]);

      if ($validator->fails()) {
        return redirect("sites/{$adomain->site_id}/edit")
        ->withErrors($validator)
        ->withInput();
      }
		
		$adomain->save();
		$adomain->massive_redirect();
		
		return redirect("sites/{$adomain->site_id}/edit");
		
		//return redirect()->route('adomains.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$adomain = Adomain::findOrFail($id);

		return view('adomains.show', compact('adomain'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$adomain = Adomain::findOrFail($id);

		return view('adomains.edit', compact('adomain'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$adomain = Adomain::findOrFail($id);

		$adomain->name = $request->input("name");

		$adomain->save();

		return redirect()->route('adomains.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$adomain = Adomain::findOrFail($id);
		$site_id = $adomain->site_id;
		
		$adomain->massive_delete_redirect();
		$adomain->delete();
		
		return redirect("sites/{$site_id}/edit");
		
		//return redirect()->route('adomains.index')->with('message', 'Item deleted successfully.');
	}

}
