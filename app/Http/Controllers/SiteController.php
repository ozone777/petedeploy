<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Log;
use App\Site;
use App\Adomain;
use App\Action;
use App\Secure;
use App\User;
use Input;
use Response;
use App\App;
use Validator;
use App\Option;
use DB;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class SiteController extends Controller {

	
	public function __construct()
	    {
	        #$this->middleware('csrf');
	        $this->middleware('auth');
			
	    }
		
			
	public function get_cms_info(){
			$secure = new Secure();
			$site = Site::findOrFail(Input::get('id'));
		
			$user_decrypt = $secure->mc_decrypt($site->cms_user);
			$password_decrypt = $secure->mc_decrypt($site->cms_password);
		
		  Log::info('Site IDX' . $site->id);
		 
		  return response()->json(['cms_user' => $user_decrypt,"cms_password" => $password_decrypt]);	 
		  
	}
	
	public function get_db_info(){
	  $secure = new Secure();
	  $site = Site::findOrFail(Input::get('id'));
		
	  $db_name_decrypt = $secure->mc_decrypt($site->db_name);
	  $db_user_decrypt = $secure->mc_decrypt($site->db_user);
	  $db_password_decrypt = $secure->mc_decrypt($site->db_password);
		
	  return response()->json(['db_name' => $db_name_decrypt,"db_user" => $db_user_decrypt, "db_password" => $db_password_decrypt]);	 
	  #return response()->json(['ok' => "ok"]);
	  #return response()->json(['name' => 'Abigail', 'state' => 'CA']);
	}
	
	public function export(){
		
		Log::info('EXPORT' . Input::get('id'));
		
		$site = Site::findOrFail(Input::get('id'));
		
		$site->export_massive();
		$exporturl = $site->file_to_download;
		
		$sites = Site::orderBy('id', 'desc')->paginate(10);
		
		return view('sites.index', compact('sites','exporturl'));
	  
	}
	
	public function trash(){
		$user = Auth::user();
		
		if($user->admin){
		  $sites = Site::onlyTrashed()->orderBy('id', 'desc')->paginate(10);
		}else{
		  $sites = Site::onlyTrashed()->orderBy('id', 'desc')->where('user_id', $user->id)->paginate(10);	
		}
		
		return view('sites.trash', compact('sites'));
	  
	}
	
	
	public function suspend(){
		
		$site = Site::findOrFail(Input::get('id'));
		$site->suspend = true;
		$site->massive_suspend();
		$site->save();
		
		return redirect()->route('sites.index')->with('message', 'Item updated successfully.');
	  
	}
	
	public function site_continue(){
		
		$site = Site::findOrFail(Input::get('id'));
		$site->suspend = false;
		
		$site->massive_continue();
		
		$site->save();
		
		return redirect()->route('sites.index')->with('message', 'Item updated successfully.');
	  
	}
	
	
	
	public function get_sites(){
		
		if (Input::get('app_name') == "Wordpress"){
		  //$result = DB::select('select id,name from sites where app_id = ?', [1]);
		  $result = Site::where('app_id', 1)->get();
		}else if(Input::get('app_name') == "Drupal"){
		  $result = Site::where('app_id', 2)->get();
		}else{
		  $result = Site::withTrashed()->orderBy('id', 'desc')->get(['id','name','url']);
		}
		
	    return response()->json($result);
	  
	}
	
	public function restore(){
		$site = Site::withTrashed()->findOrFail(Input::get('id'));
		$exporturl = "";
		$site->url = str_replace("massivetrash","",$site->url);
		$site->massive_restore();
		$site->restore();
		
		$user = Auth::user();
		if($user->admin){
		  $sites = Site::orderBy('id', 'desc')->paginate(10);
		}else{
		  #global template sites
		  $sites = Site::orderBy('id', 'desc')->where('user_id', $user->id)->paginate(10);	
		}
		
		return view('sites.index', compact('sites','exporturl'));
	}
	
	public function addkeys(){
		
		Log::info('EXPORT' . Input::get('id'));
		
		$site = Site::findOrFail(Input::get('id'));
		
		$secure = new Secure();
	
		$site->cms_user = $secure->mc_decrypt($site->cms_user);
		$site->cms_password = $secure->mc_decrypt($site->cms_password);
		
		return view('sites.addkeys', compact('site'));
	  
	}
	
	public function add_alias(){
		
		Log::info('EXPORT' . Input::get('id'));
		
		$site = Site::findOrFail(Input::get('id'));
		
		$secure = new Secure();
	
		$site->cms_user = $secure->mc_decrypt($site->cms_user);
		$site->cms_password = $secure->mc_decrypt($site->cms_password);
		
		return view('sites.addkeys', compact('site'));
	  
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$user = Auth::user();
		if($user->admin){
		  $sites = Site::orderBy('id', 'desc')->paginate(10);
		}else{
		  #global template sites
		  $sites = Site::orderBy('id', 'desc')->where('user_id', $user->id)->paginate(10);	
		}
		$first_time = "";
		$first_time = session('first_time');		
		$exporturl = "";
		
		return view('sites.index', compact('sites','exporturl','current_user','first_time'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{   
		#Url Template
		$url_template = Site::UrlTemplate();
		$drupal_support = Site::DrupalSupport();
		
		$actions = Action::orderBy('id', 'desc')->paginate(10);
		$apps = App::orderBy('id', 'desc')->paginate(10);
		$root = shell_exec('pwd');
		$root = str_replace("/massive/public","",$root);
		
		//$sites_count = Site::count();
		//Utilizamos el ultimo record de la query ordenando ASC
		//Para funciones 
		
		
		$url_template_string = Site::getUrlTemplateWithID();
		
		return view('sites.create',compact('actions','apps','output','url_template','drupal_support','sites_count','url_template_string'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		
		$site = new Site();
		$user = Auth::user();
		
		if($request->input("url_template") == "true"){
		  $url_template = Option::where('option_name','url_template')->first()->option_value;	
		}else{
		  $url_template = false;
	    }
		
		$appid= App::where('name',$request->input("app_name"))->first()->id;
		$actionid= Action::where('name',$request->input("action_name"))->first()->id;
		
		$site->app_name = $request->input("app_name");
		$site->action_name = $request->input("action_name");
		
		$site->to_clone_project = $request->input("to_clone_project");
		$site->name = $request->input("name");
		$site->to_import_project = $request->input("to_import_project");
        
		$site->action_id = $actionid;
		$site->app_id = $appid;
		$site->user_id = $user->id;
		
		$site->url = "template";
		
		if($request->file('filem')!= ""){
			
			$file = $request->file('filem');
	        // SET UPLOAD PATH
	        $destinationPath = 'uploads';
	         // GET THE FILE EXTENSION
	        $extension = $file->getClientOriginalExtension();
	         // RENAME THE UPLOAD WITH RANDOM NUMBER
	        $fileName = rand(11111, 99999) . '.' . "tar.gz";
	         // MOVE THE UPLOADED FILES TO THE DESTINATION DIRECTORY
	        $upload_success = $file->move($destinationPath, $fileName);
			$site->zip_file_url = $fileName;
		}
	
		#$site->output = shell_exec(Site::massive_exec_shell($site));
		
		if($url_template){
  	      $validator = Validator::make($request->all(), [
  	               'name' =>  array('required', 'regex:/^[a-zA-Z0-9-_]+$/','unique:sites'),
  	      ]);

  	      if ($validator->fails()) {
  	        return redirect('sites/create')
  	        ->withErrors($validator)
  	        ->withInput();
  	      }
		}else{
			
	      $validator = Validator::make($request->all(), [
				   'name' =>  array('required', 'regex:/^[a-zA-Z0-9-_]+$/','unique:sites'),
	               'url' => 'required|unique:sites',
	      ]);

	      if ($validator->fails()) {
	        return redirect('sites/create')
	        ->withErrors($validator)
	        ->withInput();
	      }
		
		}
		
		if($site->save()){
			
			if($request->input("url")){
				$site->url = $request->input("url");
			}else{
			    $site->url = Site::getUrlTemplateWithID();
			}	
			
		  $site->massive_logic();
		
		}
		
		return redirect()->route('sites.show', [$site->id]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(Request $request, $id)
	{
		$site = Site::findOrFail($id);
		$root = shell_exec('pwd');
		
		$root = shell_exec('pwd');
		$root = str_replace("/massive/public","",$root);
		
		$output = shell_exec('scripts/test -u jsmith -p notebooks -d 10-20-2011 -f pdf');
		
		#$mysql_bin = Option::where('option_name','mysql_bin')->first()->option_value;
		
		#Se puede encriptar
		$request->session()->put('os', 'centos7');
		
		return view('sites.show', compact('site','output'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//$adomains = Adomain::orderBy('id', 'desc')->get();
		
		
		$current_user = Auth::user();
		
		$secure = new Secure();
		
		
		$site = Site::findOrFail($id);
		$adomains = Adomain::orderBy('id', 'desc')->where('site_id', $site->id)->get();	
		$user_decrypt = $secure->mc_decrypt($site->cms_user);
		$password_decrypt = $secure->mc_decrypt($site->cms_password);
		
        $users = User::orderBy('id', 'desc')->get();
		
		
		return view('sites.edit', compact('site','users','current_user','user_decrypt','password_decrypt','adomains'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$site = Site::findOrFail($id);
	
	    if(($request->input("url") != "") & ($request->input("url") != $site->url)){
			
	    	$past_url = $site->url;
			$site->url = $request->input("url");
			
		    $validator = Validator::make($request->all(), [
		               'url' => 'required|unique:sites',
		    ]);

		    if ($validator->fails()) {
		      return redirect("sites/{$site->id}/edit")
		      ->withErrors($validator)
		      ->withInput();
		    }
			
		  $site->massive_move($past_url);
	    }
		
		$secure = new Secure();
		
		if($request->input("cms_user") != ""){
		  #$site->cms_user = $request->input("cms_user");
		  $site->cms_user = $secure->mc_encrypt($request->input("cms_user"));
		}
		if($request->input("cms_password") != ""){
		  #$site->cms_password = $request->input("cms_password");
		  $site->cms_password = $secure->mc_encrypt($request->input("cms_password"));
		}
		
		if($request->input("user_id") != ""){
		  $site->user_id = $request->input("user_id");
		}
		
		if($request->input("template") != ""){
		  $site->template = true;
		}else{
		  $site->template = false;
		}
		
		$site->save();

		return redirect()->route('sites.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$site = Site::findOrFail($id);
		//if($site->action_id !=5){
		  $site->url = $site->url . "massivetrash";
		  $site->massive_delete();
		  $site->save();
	    //}
		Log::info('Delete output' . $site->output);
		
		$site->delete();
		
		return redirect()->route('sites.index')->with('message', 'Item deleted successfully.');
	}
	
	

}
