<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Action;
use Illuminate\Http\Request;

class ActionController extends Controller {

	public function __construct()
	    {
	        $this->middleware('auth');
	    }
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$actions = Action::orderBy('id', 'desc')->paginate(10);

		return view('actions.index', compact('actions'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('actions.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$action = new Action();

		$action->name = $request->input("name");
        $action->body = $request->input("body");

		$action->save();

		return redirect()->route('actions.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$action = Action::findOrFail($id);

		return view('actions.show', compact('action'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$action = Action::findOrFail($id);

		return view('actions.edit', compact('action'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$action = Action::findOrFail($id);

		$action->name = $request->input("name");
        $action->body = $request->input("body");

		$action->save();

		return redirect()->route('actions.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$action = Action::findOrFail($id);
		$action->delete();

		return redirect()->route('actions.index')->with('message', 'Item deleted successfully.');
	}

}
