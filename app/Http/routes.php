<?php
use App\Option;
use App\Site;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('sites/testing', 'SiteController@testing');

#Route::get('sites/restart', 'SiteController@restart');

Route::get('sites/get_db_info', 'SiteController@get_db_info');

Route::get('sites/get_cms_info', 'SiteController@get_cms_info');

Route::get('sites/export','SiteController@export');

Route::get('sites/addkeys','SiteController@addkeys');

Route::get('sites/add_alias','SiteController@add_alias');

Route::get('sites/get_sites','SiteController@get_sites');

Route::get('sites/trash','SiteController@trash');

Route::get('sites/suspend','SiteController@suspend');

Route::get('sites/site_continue','SiteController@site_continue');

Route::get('sites/restore','SiteController@restore');

Route::resource("sites","SiteController");

Route::resource("options","OptionController");

Route::resource("apps","AppController"); 

Route::resource("actions","ActionController");

Route::resource("adomains","AdomainController");

Route::resource("apps_actions","AppsActionController");

Route::get('contact', 'WelcomeController@contact');

#Route::get('/', function () {
#    return view('sites');
#});

#Route::get('/', 'UploadController@index');
#Route::post('upload/add', 'UploadController@uploadFiles');


#Route::get('/testing', function () {
#    return response()->json(['name' => 'Abigail', 'state' => 'CA']);
#});

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

Route::get('users', 'AdminController@list_users');
Route::get('edit_user', 'AdminController@edit_user');
Route::get('force_delete', 'AdminController@force_delete');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');


Route::get('/get_domains', function () {
	
	$sites = Site::orderBy('id', 'desc')->get();
    return response()->json($sites);
});

Route::get('/get_massives', function () {
	$app_root = Option::where('option_name','app_root')->first()->option_value;
	
	$result = shell_exec("find {$app_root} -maxdepth 1 -type f -name '*.tar.gz'");
	#$result = $result . shell_exec("find {$app_root}/Massive/public/uploads -maxdepth 1 -type f -name '*.tar.gz'");
	
	$result = str_replace("{$app_root}/","",$result);
	$result = str_replace('/',"",$result);
	#$aux = str_replace("/wp-config.php","",$aux);
	#$result = explode("\n",$aux);
	$result = explode("\n",$result);
    return response()->json($result);
});


Route::get('/', array('as' => 'sites.index', 'uses' => 'SiteController@index'));
Route::get('/home', array('as' => 'sites.index', 'uses' => 'SiteController@index'));


