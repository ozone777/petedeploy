<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Log;
use App\Secure;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;


class Site extends Model
{
    //
    use SoftDeletes;

    protected $dates = ['deleted_at'];
	
	
    public function user()
       {
           return $this->belongsTo('App\User');
       }
	
    public function app()
       {
           return $this->belongsTo('App\App');
       }
	   
    public function action()
    {
              return $this->belongsTo('App\Action');
    }
	
    public static function UrlTemplate()
    {
		#Get the URL template
		if(Option::where('option_name','url_template')->first()){
		  return true;
		}else{
		  return false;
		}
    }
	
    public static function DrupalSupport()
    {
		#Get the URL template
		$record = Option::where('option_name','drupal')->first();
		if($record){
			if($record->option_value == "true"){
				return true;
			}else{
				return false;
			}
		}else{
		  return false;
		}
    }
	
	//Este metodo me devuelve la url template del dominio si la tiene
    public function getUrlTemplate()
    {
		
		
		$url_temp_aux = Option::where('option_name','url_template')->first();
		if($url_temp_aux){
		  $url_template = Option::where('option_name','url_template')->first()->option_value;
		  $url_template = str_replace("[i]",$this->id,$url_template);
		}else{
		  $url_template = false;
		}
		return $url_template;
    }
	
    public static function getUrlTemplateWithID()
    {
		#Get the URL template
		#Get the URL template
		$object = DB::table('sites')->orderBy('id', 'desc')->first();
		if($object){
			$id = $object->id;
			$id = $id + 1;
		}else{
			$id = 1;
		}
		
		
		$url_temp_aux = Option::where('option_name','url_template')->first();
		if($url_temp_aux){
		  $url_template = Option::where('option_name','url_template')->first()->option_value;
		  $url_template = str_replace("[i]",$id,$url_template);
		}else{
		  $url_template = false;
		}
		return $url_template;
    }
	
	public function massive_restore(){
		
		$secure = new Secure(); 
		$site_url = $this->url;	
        $site_name = $this->name;
		
		$db_root_pass = Option::where('option_name','db_root_pass')->first()->option_value;
		$db_root_pass = $secure->mc_decrypt($db_root_pass);
		
		$os = Option::where('option_name','os')->first()->option_value;
	    $app_root = Option::where('option_name','app_root')->first()->option_value;
        $mysql_bin = Option::where('option_name','mysql_bin')->first()->option_value;
	    $server_conf = Option::where('option_name','server_conf')->first()->option_value;
		$mysqlcommand = $mysql_bin . "mysql";
		$app_name = $this->app_name ;
		$id = $this->id;
		
		$command = "sh {$app_root}/Pete/scripts/restore.sh -n {$site_name} -p {$db_root_pass} -r {$app_root} -m {$mysqlcommand} -a {$server_conf} -v {$os} -w {$app_name} -s {$id} -u {$site_url}";
		
		Log::info('Pete Command: ' . $command);
		
		$this->output = shell_exec($command);
		$this->restart_server();
	}
	
	public function export_massive(){
	  $secure = new Secure(); 	
      $site_url = $this->url;
      $site_name = $this->name;
	  $app_root = Option::where('option_name','app_root')->first()->option_value;
      $mysql_bin = Option::where('option_name','mysql_bin')->first()->option_value;
	  $server_conf = Option::where('option_name','server_conf')->first()->option_value;
	  $db_root_pass = Option::where('option_name','db_root_pass')->first()->option_value;
	  $db_root_pass = $secure->mc_decrypt($db_root_pass);
	  $os = Option::where('option_name','os')->first()->option_value;
	  $os_version = Option::where('option_name','os_version')->first()->option_value;
	  $app_name = $this->app_name ;
	  $mysqldump = $mysql_bin . "mysqldump";
	  $id = $this->id;
	  
	  $command = "sh {$app_root}/Pete/scripts/export.sh -n {$site_name} -p {$db_root_pass} -r {$app_root} -m {$mysqldump} -v {$os} -w {$app_name} -u {$site_url}";
	  
	  $this->file_to_download = "export/{$site_name}.tar.gz";
	  
	  Log::info('Pete Command: ' . $command);
	  
	  $outputaux = shell_exec($command);
	  Log::info('Pete Output exec: ' . $outputaux);
	}
	
	public function massive_move($past_url){
	    $secure = new Secure();
        $site_name = $this->name;
		$site_url = $this->url;
		
		$db_root_pass = Option::where('option_name','db_root_pass')->first()->option_value;
		$db_root_pass = $secure->mc_decrypt($db_root_pass);
		
		$os = Option::where('option_name','os')->first()->option_value;
	    $app_root = Option::where('option_name','app_root')->first()->option_value;
        $mysql_bin = Option::where('option_name','mysql_bin')->first()->option_value;
	    $server_conf = Option::where('option_name','server_conf')->first()->option_value;
		$mysqlcommand = $mysql_bin . "mysql";
		$mysqldump = $mysql_bin . "mysqldump";
		$app_name = $this->app_name ;
		$id = $this->id;
		
	    $wpkey1 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey2 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey3 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey4 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey5 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey6 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey7 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey8 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
		
	    $db_name = "db_" . substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
	    $db_user = "usr_" . substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
	    $db_user_pass = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);

	    $command = "sh {$app_root}/Pete/scripts/move.sh -n {$site_name} -u {$site_url} -p {$db_root_pass} -r {$app_root} -m {$mysqlcommand} -q {$mysqldump} -a {$server_conf}  -x {$db_name} -y {$db_user} -z {$db_user_pass} -b {$wpkey1} -c {$wpkey2} -d {$wpkey3} -e {$wpkey4} -f {$wpkey5} -g {$wpkey6} -h {$wpkey7} -i {$wpkey8} -v {$os} -w {$app_name} -s {$id} -l {$past_url}";
		
		Log::info('Pete Command: ' . $command);

		$this->output = shell_exec($command);
		
		$this->restart_server();
	   
	}
	
	public function massive_delete(){
		$secure = new Secure();
        $site_name = $this->name;
		
		$db_root_pass = Option::where('option_name','db_root_pass')->first()->option_value;
		$db_root_pass = $secure->mc_decrypt($db_root_pass);
		
		$os = Option::where('option_name','os')->first()->option_value;
	    $app_root = Option::where('option_name','app_root')->first()->option_value;
        $mysql_bin = Option::where('option_name','mysql_bin')->first()->option_value;
	    $server_conf = Option::where('option_name','server_conf')->first()->option_value;
		$mysqlcommand = $mysql_bin . "mysql";
		$app_name = $this->app_name ;
		$id = $this->id;
		
		$command = "sh {$app_root}/Pete/scripts/delete.sh -n {$site_name} -p {$db_root_pass} -r {$app_root} -m {$mysqlcommand} -a {$server_conf} -v {$os} -w {$app_name} -s {$id}";
		
		Log::info('Pete Command: ' . $command);
		
		$this->output = shell_exec($command);
		
		$this->restart_server();
		
	}
	
	
	public function massive_suspend(){
		$secure = new Secure();
        $site_name = $this->name;
		
		$db_root_pass = Option::where('option_name','db_root_pass')->first()->option_value;
		$db_root_pass = $secure->mc_decrypt($db_root_pass);
		
		$os = Option::where('option_name','os')->first()->option_value;
	    $app_root = Option::where('option_name','app_root')->first()->option_value;
        $mysql_bin = Option::where('option_name','mysql_bin')->first()->option_value;
	    $server_conf = Option::where('option_name','server_conf')->first()->option_value;
		$mysqlcommand = $mysql_bin . "mysql";
		$app_name = $this->app_name ;
		$id = $this->id;
		
		$server_conf = Option::where('option_name','server_conf')->first()->option_value;
		$site_url = $this->url;
		
		$command = "sh {$app_root}/Pete/scripts/suspend.sh -n {$site_name} -p {$db_root_pass} -r {$app_root} -m {$mysqlcommand} -a {$server_conf} -v {$os} -w {$app_name} -s {$id} -a {$server_conf} -u {$site_url}";
		
		Log::info('Pete Command: ' . $command);
		
		$this->output = shell_exec($command);
		
		$this->restart_server();
		
	}
	
	public function massive_continue(){
		
		$secure = new Secure(); 
		$site_url = $this->url;	
        $site_name = $this->name;
		
		$db_root_pass = Option::where('option_name','db_root_pass')->first()->option_value;
		$db_root_pass = $secure->mc_decrypt($db_root_pass);
		
		$os = Option::where('option_name','os')->first()->option_value;
	    $app_root = Option::where('option_name','app_root')->first()->option_value;
        $mysql_bin = Option::where('option_name','mysql_bin')->first()->option_value;
	    $server_conf = Option::where('option_name','server_conf')->first()->option_value;
		$mysqlcommand = $mysql_bin . "mysql";
		$app_name = $this->app_name ;
		$id = $this->id;
		
		$command = "sh {$app_root}/Pete/scripts/continue.sh -n {$site_name} -p {$db_root_pass} -r {$app_root} -m {$mysqlcommand} -a {$server_conf} -v {$os} -w {$app_name} -s {$id} -u {$site_url}";
		
		Log::info('Pete Command: ' . $command);
		
		$this->output = shell_exec($command);
		$this->restart_server();
	}
	
	public function restart_server(){
	   
		$os = Option::where('option_name','os')->first()->option_value;
		$os_version = Option::where('option_name','os_version')->first()->option_value;
	   
   	   if(($os == "linux") && ($os_version == "centos7")){
   	  	 exec('sudo /bin/systemctl reload httpd.service');
   	   }else if ($os == "osx"){
   		 exec('/Applications/MAMP/Library/bin/apachectl graceful');
   	   }  
	}
	
	
	public function massive_logic(){
		
		
        $site_url = $this->url;
        $site_name = $this->name;
		
	    $app_root = Option::where('option_name','app_root')->first()->option_value;
        $mysql_bin = Option::where('option_name','mysql_bin')->first()->option_value;
	    $server_conf = Option::where('option_name','server_conf')->first()->option_value;
		$os = Option::where('option_name','os')->first()->option_value;
		$os_version = Option::where('option_name','os_version')->first()->option_value;
		
		//$apache_version = Option::where('option_name','apache_version')->first()->option_value;
		//$apache_version = str_replace(".","",$apache_version);
		//$apache_version = substr($apache_version,0,3);
			
	    $db_name = "db_" . substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
	    $db_user = "usr_" . substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
	    $db_user_pass = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
	
	    $wpkey1 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey2 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey3 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey4 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey5 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey6 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey7 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey8 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
		
		#$mysqldump = Option::where('option_name','mysqldump')->first()->option_value;
		$zip_file_url = $this->zip_file_url; 
	    
		
	    $mysqldump = $mysql_bin . "mysqldump";
		$mysqlcommand = $mysql_bin . "mysql";
		$app_name = $this->app_name ;
		$to_import_project= $this->to_import_project;
		$id = $this->id;
				
		$secure = new Secure();
		$this->db_name = $secure->mc_encrypt($db_name);
		$this->db_user = $secure->mc_encrypt($db_user);
		$this->db_password = $secure->mc_encrypt($db_user_pass);
		
		$db_root_pass = Option::where('option_name','db_root_pass')->first()->option_value;
		$db_root_pass = $secure->mc_decrypt($db_root_pass);
		
		if($this->action_name == "New"){
		  $command = "sh {$app_root}/Pete/scripts/new.sh -n {$site_name} -u {$site_url} -p {$db_root_pass} -r {$app_root} -m {$mysqlcommand} -a {$server_conf} -x {$db_name} -y {$db_user} -z {$db_user_pass} -b {$wpkey1} -c {$wpkey2} -d {$wpkey3} -e {$wpkey4} -f {$wpkey5} -g {$wpkey6} -h {$wpkey7} -i {$wpkey8} -v {$os} -w {$app_name} -s {$id}";
		
		}else if($this->action_name == "Import"){
		   
		  $command = "sh {$app_root}/Pete/scripts/import.sh -n {$site_name} -u {$site_url} -z {$zip_file_url} -p {$db_root_pass} -r {$app_root} -m {$mysqlcommand} -a {$server_conf} -x {$db_name} -y {$db_user} -l {$db_user_pass} -b {$wpkey1} -c {$wpkey2} -d {$wpkey3} -e {$wpkey4} -f {$wpkey5} -g {$wpkey6} -h {$wpkey7} -i {$wpkey8} -v {$os} -w {$app_name} -s {$id}";
		  
		}else if($this->action_name == "Clone"){
			
		  $to_clone_project = $this->to_clone_project;
		  $to_clone_site_object = Site::where('name',"$to_clone_project")->first();
		  $this->cms_user = $to_clone_site_object->cms_user;
		  $this->cms_password = $to_clone_site_object->cms_password;
		  $to_clone_site_url = Site::where('name',"$to_clone_project")->first()->url;

		  $command = "sh {$app_root}/Pete/scripts/clone.sh -n {$site_name} -o {$to_clone_project} -u {$site_url} -p {$db_root_pass} -r {$app_root} -m {$mysqlcommand} -q {$mysqldump} -a {$server_conf}  -x {$db_name} -y {$db_user} -z {$db_user_pass} -b {$wpkey1} -c {$wpkey2} -d {$wpkey3} -e {$wpkey4} -f {$wpkey5} -g {$wpkey6} -h {$wpkey7} -i {$wpkey8} -v {$os} -w {$app_name} -s {$id} -l {$to_clone_site_url}";
			
		}	

	   Log::info('Pete Command: ' . $command);
		
	   $this->output = shell_exec($command);
		
	   $this->save();
	   
	   $this->restart_server();
		
	}
	
}
