<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Option;
use App\Secure;

class AddPass extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'addpass {pass?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add DB password to .env file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
	
	
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
		//$this->comment("Hola Pedro {$this->argument('pass')}" );
		$key = $this->argument('pass');
		$secure = new Secure();
		$db_pass_encrypted = $secure->mc_encrypt($key);
		
		$option = new Option();
		$option->option_name = "db_root_pass";
        $option->option_value = $db_pass_encrypted;
		$option->save();
		
    }
}
