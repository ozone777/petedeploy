<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Option;

class AddHexKey extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'addhexkey';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
		$key = '';
		$app_root = Option::where('option_name','app_root')->first()->option_value;
		$key = bin2hex(openssl_random_pseudo_bytes(32));
		echo $key;
		$command = "sh {$app_root}/Pete/scripts/add_hex.sh -k {$key} -r {$app_root}/Pete";
		shell_exec($command);
    }
}
