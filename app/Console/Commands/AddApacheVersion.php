<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Option;

class AddApacheVersion extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'addapacheversion';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add the version of the Apache Server.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
		$bin = Option::where('option_name','apache_bin')->first()->option_value;
		$app_root = Option::where('option_name','app_root')->first()->option_value;
		$command = "sh {$app_root}/Pete/scripts/apache_version.sh -b {$bin}";
		$apache_version = shell_exec($command);
		
		$option = new Option();
		$option->option_name = "apache_version";
        $option->option_value = $apache_version;

		$option->save();
    }
}
