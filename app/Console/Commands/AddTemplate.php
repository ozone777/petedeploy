<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Option;

class AddTemplate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'addtemplate {template?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add template';

    /**
     * Create a new command instance.
     *
     * @return void
     */
	
	
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       	
		$option = new Option();

		$option->option_name = "url_template";
        $option->option_value = $this->argument('template');

		$option->save();
		
    }
}
