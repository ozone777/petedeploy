<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
		\App\Console\Commands\AddPass::class,
		\App\Console\Commands\AddOsVersion::class,
		\App\Console\Commands\AddFields::class,
		\App\Console\Commands\AddFields::class,
		\App\Console\Commands\AddHexKey::class,
		\App\Console\Commands\FirstAdmin::class,
		\App\Console\Commands\AddTemplate::class,
		\App\Console\Commands\AddApacheVersion::class,
		
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('inspire')
                 ->hourly();
    }
}
