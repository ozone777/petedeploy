<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Log;
use App\Option;

class Secure
{
   # define('key',''); 
	
	public $o_key = "";
	public $db_key = "";
	
	function __construct() {
	   $app_root = Option::where('option_name','app_root')->first()->option_value;
	   $command = "sh {$app_root}/Pete/scripts/get_hex.sh -r {$app_root}";
	   $this->o_key = shell_exec($command); 
	   $this->o_key = preg_replace( "/\r|\n/", "", $this->o_key );
	   
	   $command = "sh {$app_root}/Pete/scripts/get_db_key.sh -r {$app_root}";
	   $this->db_key = shell_exec($command); 
	   $this->db_key = preg_replace( "/\r|\n/", "", $this->db_key );
	}
	
	public function mc_encrypt($encrypt){
	    $encrypt = serialize($encrypt);
	    $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC), MCRYPT_DEV_URANDOM);
	    $key = pack('H*', $this->o_key);
	    $mac = hash_hmac('sha256', $encrypt, substr(bin2hex($key), -32));
	    $passcrypt = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $encrypt.$mac, MCRYPT_MODE_CBC, $iv);
	    $encoded = base64_encode($passcrypt).'|'.base64_encode($iv);
	    return $encoded;
	}
	// Decrypt Function
	public function mc_decrypt($decrypt){
	    $decrypt = explode('|', $decrypt.'|');
	    $decoded = base64_decode($decrypt[0]);
	    $iv = base64_decode($decrypt[1]);
	    if(strlen($iv)!==mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC)){ return false; }
	    $key = pack('H*', $this->o_key);
	    $decrypted = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $decoded, MCRYPT_MODE_CBC, $iv));
	    $mac = substr($decrypted, -64);
	    $decrypted = substr($decrypted, 0, -64);
	    $calcmac = hash_hmac('sha256', $decrypted, substr(bin2hex($key), -32));
	    if($calcmac!==$mac){ return false; }
	    $decrypted = unserialize($decrypted);
	    return $decrypted;
	}	
	   
}
