<?php

namespace App;
use App\Site;
use Log;
use Illuminate\Database\Eloquent\Model;

class Adomain extends Model
{
    //
	public function massive_redirect(){
				
		$os = Option::where('option_name','os')->first()->option_value;
		$adomain_name =  $this->name;
		$site = Site::findOrFail($this->site_id); 
		$site_url = $site->url;
		$app_root = Option::where('option_name','app_root')->first()->option_value;
		$server_conf = Option::where('option_name','server_conf')->first()->option_value;
		$id = $this->id;
		$filename = str_replace(".","",$adomain_name);
		$filename = "redirect" . $filename;
			
		$command = "sh {$app_root}/Pete/scripts/add_redirect.sh -v {$os} -n {$adomain_name} -u {$site_url} -r {$app_root} -a {$server_conf} -s {$id} -x {$filename}";
		
		Log::info('Pete Command: ' . $command);
		
		$this->output = shell_exec($command);
		$this->save();
		$site->restart_server();
	}
	
	
    //
	public function massive_delete_redirect(){
				
		$os = Option::where('option_name','os')->first()->option_value;
		$adomain_name =  $this->name;
		$site = Site::findOrFail($this->site_id); 
		$site_url = $site->url;
		$app_root = Option::where('option_name','app_root')->first()->option_value;
		$server_conf = Option::where('option_name','server_conf')->first()->option_value;
		$id = $this->id;
		$filename = str_replace(".","",$adomain_name);
		
		$command = "sh {$app_root}/Pete/scripts/delete_redirect.sh -v {$os} -n {$adomain_name} -u {$site_url} -r {$app_root} -a {$server_conf} -s {$id} -x {$filename}";
		
		//Log::info('Pete Command: ' . $command);
		
		$this->output = shell_exec($command);
		$this->save();
		
		$site->restart_server();
	}
}
