@extends('layout')

@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-edit"></i> AppsActions / Edit #{{$apps_action->id}}</h1>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('apps_actions.update', $apps_action->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('app_id')) has-error @endif">
                       <label for="app_id-field">App_id</label>
                    <input type="text" id="app_id-field" name="app_id" class="form-control" value="{{ $apps_action->app_id }}"/>
                       @if($errors->has("app_id"))
                        <span class="help-block">{{ $errors->first("app_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('action_id')) has-error @endif">
                       <label for="action_id-field">Action_id</label>
                    <input type="text" id="action_id-field" name="action_id" class="form-control" value="{{ $apps_action->action_id }}"/>
                       @if($errors->has("action_id"))
                        <span class="help-block">{{ $errors->first("action_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('script')) has-error @endif">
                       <label for="script-field">Script</label>
                    <input type="text" id="script-field" name="script" class="form-control" value="{{ $apps_action->script }}"/>
                       @if($errors->has("script"))
                        <span class="help-block">{{ $errors->first("script") }}</span>
                       @endif
                    </div>
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('apps_actions.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection