@extends('layout')
@section('header')
<div class="page-header">
        <h1>AppsActions / Show #{{$apps_action->id}}</h1>
        <form action="{{ route('apps_actions.destroy', $apps_action->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('apps_actions.edit', $apps_action->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <form action="#">
                <div class="form-group">
                    <label for="nome">ID</label>
                    <p class="form-control-static"></p>
                </div>
                <div class="form-group">
                     <label for="app_id">APP_ID</label>
                     <p class="form-control-static">{{$apps_action->app_id}}</p>
                </div>
                    <div class="form-group">
                     <label for="action_id">ACTION_ID</label>
                     <p class="form-control-static">{{$apps_action->action_id}}</p>
                </div>
                    <div class="form-group">
                     <label for="script">SCRIPT</label>
                     <p class="form-control-static">{{$apps_action->script}}</p>
                </div>
            </form>

            <a class="btn btn-link" href="{{ route('apps_actions.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>

        </div>
    </div>

@endsection