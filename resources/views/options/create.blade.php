@extends('layout')

@section('header')
    <div class="page-header">
        <h3>Create new Option</h3>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('options.store') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group">
                       <label for="option_name-field">Option_name</label>
                    <input type="text" id="option_name-field" name="option_name" class="form-control" value="{{ old("option_name") }}"/>
                      
                    </div>
                    <div class="form-group">
                       <label for="option_value-field">Option_value</label>
                    <input type="text" id="option_value-field" name="option_value" class="form-control" value="{{ old("option_value") }}"/>
                    </div>
               
                    <button type="submit" class="btnpete">Create option</button>
                
            </form>

        </div>
    </div>
@endsection