@extends('layout')

@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-edit"></i> Sites / Add Keys #{{$site->id}}</h1>
    </div>
@endsection

@section('content')
<div id="loading_area"></div>
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('sites.update', $site->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group">
                       <label for="url-field">CMS User</label>
                    <input type="text" id="cms_user-field" name="cms_user" class="form-control" value="{{ $site->cms_user }}"/>
                      
                    </div>
					
					
                    <div class="form-group ">
                       <label for="cms_password-field">CMS Password</label>
                    <input type="text" id="cms_password-field" name="cms_password" class="form-control" value="{{ $site->cms_password }}"/>
                      
                    </div>
                    
                <div class="well well-sm">
                    <button type="submit" id ="update_button" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('sites.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>

        </div>
    </div>
	
	
	<script>
	
	var createsw = false;
	
  $("#update_button").click(function() {
	  
	  if(createsw == false){

		 $("#loading_area").html('<div id="loading_div"></div>');
	  
	  	if($('#url-field').attr('success')=="false"){
		  alert("There is a site or project with the same URL. Please change it!");
		  return false;
		  }
	  
	  }else{
	  	console.log("Wait Please!!")
		return false;
	  }
	  
	  createsw = true;
  });
	
 $('#url-field').observe_field(1, function( ) {
	
  $("#loading_area").html('<div id="loading_div"></div>'); 
    
   
   sw = 0;
  $.ajax({
        url: "/get_domains",
        type: "get",
    datatype: 'json',
        success: function(data){
			$("#loading_area").html('');
		
	        for(p in data){
				
				console.log(data[p].url);
				
				if(data[p].url == $('#url-field').val()){
					
					sw = 1;
					$('#url-field').removeClass("great_name");
					$('#url-field').addClass("wrong_name");
					$('#url-field').attr( "success", "false" );
					$('#url_error_area').html('<label id="name-field-error" class="error" for="name-field">There Site with the same domain. Please change it!</label>');
				}
				
	        }
	        if(sw == 0){
				$('#url-field').removeClass("wrong_name");
	            $('#url-field').addClass("great_name");
				$('#url-field').attr( "success", "true" );
				$('#url_error_area').html("");
	        }
			
			
		}
	});
	 
 });
	
	</script>
	
@endsection