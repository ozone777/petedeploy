@extends('layout')

@section('header')
    
@endsection

@section('content')
<div id="loading_area"></div>
    @include('error')
	
	@if($current_user->admin)
    <div class="row">
        <div class="col-md-12">
			<br /><br />
            <form action="{{ route('sites.update', $site->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
					
					
 			   		
	                <div class="form-group">
                         <label for="user_id-field">Select user</label>
							<select class="form-control" id = "user_id-field" name ="user_id" required>
						
							    @foreach($users as $user)
								@if($user->id == $site->user_id)
							      <option value="{{$user->id}}" selected>{{$user->name}}</option>
								@else
								  <option value="{{$user->id}}">{{$user->name}}</option>
								@endif
							  @endforeach
							</select>
	                 </div>
                    
           
                    <button type="submit" id ="update_button" class="btnpete">Assign to user</button>
                    
         
            </form>
			<br /><br />
        </div>
    </div>
	
	 @endif
	 
     <div class="row">
         <div class="col-md-12">
			 
			  <label for="user_id-field">Alias Domains</label>
			 
             @if($adomains->count())
                 <table class="table table-condensed table-striped">
                  

                     <tbody>
                         @foreach($adomains as $adomain)
                             <tr>
                                
                                 <td><a href="http://{{$adomain->name}}" target ='_blank'>{{$adomain->name}}</a></td>
                                 <td class="text-right">
                                     
                                    
                                     <form action="{{ route('adomains.destroy', $adomain->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                         <input type="hidden" name="_method" value="DELETE">
                                         <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                         <button type="submit" class="btn btn-xs btn-danger"> Delete</button>
                                     </form>
                                 </td>
                             </tr>
                         @endforeach
                     </tbody>
                 </table>
             @else
                 <h3 class="text-center alert alert-info">Empty!</h3>
             @endif

         </div>
     </div>
	 
     <div class="row">
         <div class="col-md-12">
	 
     <form action="{{ route('adomains.store') }}" method="POST">
         <input type="hidden" name="_token" value="{{ csrf_token() }}">
		 <input type="hidden" name="site_id" id = "site_id-field" value="{{$site->id}}">
         <div class="form-group">
               <label for="user_id-field">URL</label>
             <input type="text" id="name-field" name="name" class="form-control" value="{{ old("name") }}"/>
             </div>
         
             <button type="submit" class="btnpete">Create alias domain</button>
        
     </form>
	 
         </div>
		 
		 <br /><br />
     </div>
	 <br /><br />
    <div class="row">
        <div class="col-md-12">
			<label for="url-field">Wordpress keys</label><i>(
these keys will be stored and encrypted)</i>
            <form action="{{ route('sites.update', $site->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group">
                       <i>User</i>
                    <input type="text" id="cms_user-field" name="cms_user" class="form-control" value="{{ $user_decrypt }}"/>
                      
                    </div>
					
					
                    <div class="form-group ">
                       <i>Password</i>
                    <input type="text" id="cms_password-field" name="cms_password" class="form-control" value="{{ $password_decrypt }}"/>
                      
                    </div>
                    
                
                    <button type="submit" id ="update_button" class="btnpete">Save</button>
               
            </form>

        </div>
    </div>
	<br /><br />

	
	
@endsection