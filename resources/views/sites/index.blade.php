@extends('layout')

@section('header')


    
        <h1>
            <img alt="w00t!" src="/toonpete.png" style="height: 204px">
			
            <a class="btnpete pull-right" href="{{ route('sites.create') }}"><i class="glyphicon glyphicon-plus"></i> Create new site</a>
        </h1>


@endsection

@section('content')
    <div class="row">
		
		
        <div class="col-md-12">
			<div class="content table-responsive table-full-width">
            @if($sites->count())
                <table class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th>Id</th>
                           <th>Name</th>
                        <th>Url</th>
                        
                        <th>Action</th>
						<th>App</th>
						<th>User</th>
						
						<th>Keys</th>
						
						
						
                         <th class="text-right">Options</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($sites as $site)
                            <tr>
                                <td>{{$site->id}}</td>
                     <td>{{$site->name}}</td>           
                    <td>
					<a href="http://{{$site->url}}" target ='_blank'>{{$site->url}}</a>
					</td>
                    
                    <td>{{$site->action->name}}</td>
					<td>{{$site->app->name}}</td>
					<td>
						@if($site->user)
						{{$site->user->name}}
					    @endif
					</td>
					
					
					<td>@if(($site->cms_user) && ($site->cms_password))
						<i class="pe-7s-key"></i>
						 @endif
					</td>
                                <td class="text-right">
                                    <a class="btn btn-xs btn-default" href="{{ route('sites.show', $site->id) }}"> Show</a>
                                   <a class="btn btn-xs btn-default" role="group" href="{{ route('sites.edit', $site->id) }}"> Edit</a>
								   
								    <a class="btn btn-xs btn-default" href="/sites/export?id={{$site->id}}">Export</a>
									
									@if(!$site->suspend)
									<a class="btn btn-xs btn-default" href="/sites/suspend?id={{$site->id}}">Suspend</a>
									@else
									<a class="btn btn-xs btn-default" href="/sites/site_continue?id={{$site->id}}">Continue</a>
									@endif
									
                                    <form action="{{ route('sites.destroy', $site->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $sites->render() !!}
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif
			</div>
        </div>
    </div>
	
	@if($exporturl != "")
		<script>
		  window.location.assign("/{{$exporturl}}");
		</script>
	@endif
	
	<script type="text/javascript">
	
	
    	$(document).ready(function(){
			
        	//demo.initChartist();
			/*
        	$.notify({
            	icon: 'pe-7s-arc',
            	message: "Welcome to the future of OZONE. Welcome to <b>Massive Server</b>"

            },{
                type: 'info',
                timer: 4000
            });
			*/

    	});
		
	</script>
	

@endsection