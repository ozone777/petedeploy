            <form action="{{ route('sites.store') }}" id ="SiteForm" method="POST" enctype="multipart/form-data">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
				
                

               
					
					<input type="hidden" id = "aap_name-field" name ="app_name" value="Wordpress">
                       


                <div class="form-group">
                      
						<select class="form-control" id = "action_name-field" name ="action_name" required>
							<option value="">Select Action</option>
						    @foreach($actions as $action)
						  <option value="{{$action->name}}">{{$action->name}}</option>
						  @endforeach
						</select>
                    </div>
					
				
					
                <div class="form-group" id="name_div" style="display: none;">
                   <p> Site Name</p>
                <input type="text" id="name-field" name="name" class="form-control" value="{{ old("name") }}" required/>
                   
				   <div id="name_error_area"> 
				   </div>
                </div>

                
				@if($url_template)

                <div class="form-group" id="url_div" style="display: none;">
                  <p>URL</p>
				  
                  
				
  				 <input type="checkbox" id="url_template" name="url_template" value="true" checked> Automatic URL<br>
  				  <br />
  				
                  <input type="text" id="url-field" name="url" class="form-control" value="{{ $url_template_string }}" readonly/>
					   
				  <div id="url_error_area"> 
				  </div>
					   
                </div>
				
				@else
				
                <div class="form-group" id="url_div" style="display: none;">
                  <p>URL</p>
                  <input type="text" id="url-field" name="url" class="form-control" value="{{ old("url") }}" required/>
					   
				  <div id="url_error_area"> 
				  </div>
					   
                </div>
				
				@endif
				
			   
					
	            <div class="form-group" id="zip_file_url_div" style="display: none;">
	              <label for="zip_file_url-field">Zip File URL</label>
                  <input type="file" id="filem" name="filem">
	            </div>
				
				<div id="massive_form">
					
				</div>
				
                    
               
                    <button type="submit" id="create_button" class="btnpete">Create new site</button>
               
            </form>
			
			
			

        
	
	<script>
	
	var patt = new RegExp(/^[a-z0-9]+$/i);
	var patturl = new RegExp(/^[a-z0-9\.]+$/i);
	
	$("#url_template").change(function() {
		if($("#url-field").prop("readonly")){
			$("#url-field").prop("readonly", false);
		}else{
			$("#url-field").prop("readonly", true);
		}
		//alert("hola");
	});
	
	$("#aap_name-field").change(function() {
		form_logic($("#aap_name-field").val(),$("#action_name-field").val());
	});
	
	$("#action_name-field").change(function() {
	  form_logic($("#aap_name-field").val(),$("#action_name-field").val());
	});
	
	function form_logic(app,action){
		if((app != 0) && (action != 0)){
			hide_fields();
			//$("#name_div").show();
			
			if((app == "Wordpress") || (app == "Drupal")){
				
				if(action=="New"){
				  $("#name_div").show();
				  $("#url_div").show();	
				  $("#massive_form").html("");
				}
				else if (action=="Clone") {
				  
				  if(app == "Wordpress"){
  				  
  				  $("#loading_area").html('<div id="loading_div"></div>');
				  $.ajax({
				        url: "/sites/get_sites",
				        type: "get",
				    datatype: 'json',
					  data: { app_name: "Wordpress"},
				        success: function(data){
							$("#loading_area").html('');
							console.log(data);
							select_aux = "<label for='to_clone_project-field'>Project to clone</label>" ;
							select_aux += '<select id="to_clone_project-field" name="to_clone_project" class = "form-control">';	
							
							var arrayLength = data.length;
							for (var i = 0; i < arrayLength; i++) {	
							  select_aux +='<option value="'+data[i].name+'">'+data[i].name+'</option>';
							}
							
							select_aux +='</select><br/>';
		    				  $("#name_div").show();
		    				  $("#url_div").show();
							$("#massive_form").html(select_aux);
							$("#massive_form").show();
				        }
						
					});
				}else if(app == "Drupal"){
				
 				    $("#loading_area").html('<div id="loading_div"></div>');
 				    $.ajax({
 				        url: "/sites/get_sites",
 				        type: "get",
 				    datatype: 'json',
						data: { app_name: "Drupal"},
 				        success: function(data){
 							$("#loading_area").html('');
 							console.log(data);
 							select_aux = "" ;
							select_aux = "<label for='to_clone_project-field'>Project to clone</label>" ;
 							select_aux += '<select id="to_clone_project-field" name="to_clone_project" class = "form-control">';
 							
							var arrayLength = data.length;
							for (var i = 0; i < arrayLength; i++) {	
							  select_aux +='<option value="'+data[i].name+'">'+data[i].name+'</option>';
							}
							
 							select_aux +='</select><br/>';
 							$("#massive_form").html(select_aux);
		    				$("#name_div").show();
		    				$("#url_div").show();
							$("#massive_form").show();
 				        }
 				   });	
				  
				}		
								 
				}else if (action=="Import") {
  				    $("#name_div").show();
  				    $("#url_div").show();
  				    $("#zip_file_url_div").show();
				  
				}
				
			}
			
		}
	}
	
	function hide_fields(){
	  $("#massive_form").hide();
	  $("#name_div").hide();
	  $("#url_div").hide();
	  $("#zip_file_url_div").hide();
	  $("#db_root_pass_div").hide();
	}
	
     $('#name-field').observe_field(1, function( ) {
       if($(this).val() != ""){
		   
		   sw = 0;
          $("#loading_area").html('<div id="loading_div"></div>');
		  $.ajax({
		        url: "/sites/get_sites",
		        type: "get",
		    datatype: 'json',
		        success: function(data){
					$("#loading_area").html('');
					console.log(data);
					
					var arrayLength = data.length;
					for (var i = 0; i < arrayLength; i++) {	
					  //select_aux +='<option value="'+data[i].name+'">'+data[i].name+'</option>';
					  if(data[i].name == $('#name-field').val()){
						
						sw = 1;
						$('#name-field').removeClass("great_name");
						$('#name-field').addClass("wrong_name");
						$('#name-field').attr( "success", "false" );
						$('#name_error_area').html('<i id="name-field-error">There is a folder or project with the same name. Please change it!</i>');
					  }
					}
					
					if(patt.test($('#name-field').val())==false){
						sw = 1;
						$('#name-field').removeClass("great_name");
						$('#name-field').addClass("wrong_name");
						$('#name-field').attr( "success", "false" );
						$('#name_error_area').html('<i id="name-field-error">The name of the Site must be alphanumeric.</i>');
					}
					
			        if(sw == 0){
						$('#name-field').removeClass("wrong_name");
			            $('#name-field').addClass("great_name");
						$('#name-field').attr( "success", "true" );
						$('#name_error_area').html("");
			        }
					
		        }
				});
		     }
     });
	 
	 $('#url-field').observe_field(1, function( ) {
		
	  $("#loading_area").html('<div id="loading_div"></div>'); 

	   sw = 0;
	  $.ajax({
	        url: "/sites/get_sites",
	        type: "get",
	    datatype: 'json',
	        success: function(data){
				$("#loading_area").html('');
			
		        for(p in data){
					
					console.log(data[p].url);
					
					if(data[p].url == $('#url-field').val()){
						
						sw = 1;
						$('#url-field').removeClass("great_name");
						$('#url-field').addClass("wrong_name");
						$('#url-field').attr( "success", "false" );
						$('#url_error_area').html('<i id="name-field-error" class="error" for="name-field">There Site with the same domain. Please change it!</i>');
					}
					
		        }
				
				if(patturl.test($('#url-field').val())==false){
					sw = 1;
					$('#url-field').removeClass("great_name");
					$('#url-field').addClass("wrong_name");
					$('#url-field').attr( "success", "false" );
					$('#url_error_area').html('<i id="name-field-error">The URL must be only alphanumeric characters and dots </i>');
				}
				
		        if(sw == 0){
					$('#url-field').removeClass("wrong_name");
		            $('#url-field').addClass("great_name");
					$('#url-field').attr( "success", "true" );
					$('#url_error_area').html("");
		        }
				
				
			}
		});
		 
	 });
	 
	var createsw = false;
	 
	jQuery( document ).ready(function( $ ) {

	 $("#SiteForm").validate();
	 
	  });
	  
	  
	 // $("#create_button").one( "click", function() {
	/*
	  $("#create_button").click(function() {
		  
		  
		  if(createsw == false){
		    createsw = true;
		  }else{
		  	console.log("Wait Please!!")
			return false;
		  }
		 
	  });
		 */
		
	
	</script>