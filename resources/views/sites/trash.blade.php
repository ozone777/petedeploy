@extends('layout')

@section('header')
   
@endsection

@section('content')
    <div class="row">
		
		
        <div class="col-md-12">
			<div class="content table-responsive table-full-width">
				
            @if($sites->count())
                <table class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th>Id</th>
                           <th>Name</th>
                        <th>Url</th>
                        
                        <th>Action</th>
						<th>App</th>
						<th>User</th>
						
						<th>Keys</th>
						
						
						
                         <th class="text-right">Options</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($sites as $site)
                            <tr>
                                <td>{{$site->id}}</td>
                     <td>{{$site->name}}</td>           
                    <td>
					<a href="http://{{$site->url}}" target ='_blank'>{{$site->url}}</a>
					</td>
                    
                    <td>{{$site->action->name}}</td>
					<td>{{$site->app->name}}</td>
					<td>
						@if($site->user)
						{{$site->user->name}}
					    @endif
					</td>
					
					
					<td>@if(($site->cms_user) && ($site->cms_password))
						<img src="check-mark-10-xxl.png" alt="Mountain View" style="width:30px;height:30px;">
						 @endif
					</td>
					

                                <td class="text-right">
                                    
								    <a class="btn btn-xs btn-primary" href="/sites/restore?id={{$site->id}}"></i>Restore</a>
									
									@if($current_user->admin)
									<a class="btn btn-xs btn-danger" href="/force_delete?id={{$site->id}}"></i>Force delete</a>
									 @endif
									
                                   
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $sites->render() !!}
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif
			</div>
        </div>
    </div>

@endsection