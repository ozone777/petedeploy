@extends('layout')
@section('header')
    <div class="row">
        <div class="col-md-12">
       
        <form action="{{ route('sites.destroy', $site->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('sites.edit', $site->id) }}">Edit</a>
		
                <button type="submit" class="btn btn-danger">Delete</button>
            </div>
        </form>
		
		<div id="loading_area">
	
		</div>
		</div>
		</div>
		

@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
			
			
            <form action="#">
                <div class="form-group">
           
					<p>Id: {{$site->id}}</p>
                </div>
				
                <div class="form-group">
                     <p>Url:
					
					 <a href="http://{{$site->url}}" target="_blank">{{$site->url}}</a></p>
                     
                </div>
				
                <div class="form-group">
                   <p> Name: {{$site->name}}</p>
                </div>
				
                <div class="form-group">
                     <p>DB Info:
                     <a class="btn btn-xs btn-primary" id ="show_db_info" href="/"> View</a></p>
					 <div id ="db_info">
					   
					</div>
                </div>
				
                <div class="form-group">
                     <p>CMS Info:
                     <a class="btn btn-xs btn-primary" id ="show_cms_info" href="/"> View</a></p>
					 <div id ="cms_info">
					   
					</div>
                </div>
				
                <div class="form-group">
                     <p>Output: </p>
                     <pre>{{$site->output}}</pre>
                </div>
				
            </form>

        </div>
    </div>
	
	<script>
	
	$(document).ready(function(){
	
	$( "#show_db_info" ).click(function() {
	  $("#loading_area").html('<div id="loading_div"></div>');
  	  $.ajax({
  	        url: "/sites/get_db_info?id={{$site->id}}",
  	        type: "get",
  	        datatype: 'json',
  	        success: function(data){
			   $("#loading_area").html('');
			   aux = "<p>";
			   aux +="<strong>DB Name: </strong>"+data['db_name']+"<br/>";
			   aux +="<strong>DB User: </strong>"+data['db_user']+"<br/>";
			   aux +="<strong>DB Password: </strong>"+data['db_password']+"<br/>";
			   aux += "</p>";
			   
			   $("#db_info").html(aux);
  	        }
				
  	  });
		
		return false;
	});
	
	
	$( "#show_cms_info" ).click(function() {
	  $("#loading_area").html('<div id="loading_div"></div>');
  	  $.ajax({
  	        url: "/sites/get_cms_info?id={{$site->id}}",
  	        type: "get",
  	        datatype: 'json',
  	        success: function(data){
			   $("#loading_area").html('');
			   aux = "<p>";
			   aux +="<strong>DB User: </strong>"+data['cms_user']+"<br/>";
			   aux +="<strong>DB Password: </strong>"+data['cms_password']+"<br/>";
			   aux += "</p>";
			   
			   $("#cms_info").html(aux);
  	        }
				
  	  });
		
		return false;
	});
	
	
	/*	
	  $.ajax({
	        url: "/sites/restart",
	        type: "get",
	        datatype: 'json',
	        success: function(data){
	          //alert("success");			
	        }
				
	  });
	
	*/
		
	});
	
	</script>

@endsection