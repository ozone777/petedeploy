@extends('layout')

@section('header')
    <div class="page-header">
    <h3> Create new site
	</h3>
	
	<div id="loading_area"></div>
	
    </div>
	
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">
			
			@if($drupal_support)
			
                @include('sites/multiplecms')
			
			@else
			
				@include('sites/pete')
			
			@endif

             

        </div>
    </div>
	
@endsection