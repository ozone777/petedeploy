@section('header')
    <div class="page-header clearfix">
        <h1>
            <img src="/massive.png" alt="Massive Server" style="width:70px;height:70px;">
            <a class="btn btn-success pull-right" href="{{ route('actions.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
        </h1>

    </div>
@endsection