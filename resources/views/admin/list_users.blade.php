@extends('layout')

@section('header')
    <div class="page-header clearfix">
        <h1>
            
            <a class="btn btn-success pull-right" href="/auth/register"><i class="glyphicon glyphicon-plus"></i> Create</a>
        </h1>

    </div>
@endsection

@section('content')
    <div class="row">
		
		
        <div class="col-md-12">
            @if($users->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                        <th>Id</th>
                        <th>Name</th>
						<th>Admin</th>
                        <th>Email</th>
                     
                    </thead>

                    <tbody>
                        @foreach($users as $user)
                            <tr>
                      <td>{{$user->id}}</td>
                     <td>{{$user->name}}</td>  
					 <td>{{$user->admin}}</td>           
                   	 <td>{{$user->email}}</td>           
                    
     
					
                                <td class="text-right">
                                   	
									<a class="btn btn-xs btn-warning" role="group" href="/edit_user?id={{$user->id}}">Edit</a>
									
                                    <form action="{{ route('sites.destroy', $user->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger">Delete</button>
                                    </form>
									
									
									
                                </td>
								
								
								
                            </tr>
                        @endforeach
                    </tbody>
                </table>
             
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>
	

@endsection