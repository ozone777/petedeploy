@extends('layout')

@section('header')
    <div class="page-header">
        <h3>Create new Option</h3>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">
			
		  	<form method="POST" action="/auth/register">
		  	    {!! csrf_field() !!}
				
				<div class="form-group">
		      	  	<label for="fieldUser">Username</label>
		  			<input type="text" name="name" class="input" value="{{ old('name') }}">
	
		          </div>
		          
				 <div class="form-group">
					 <label for="fieldUser">Email</label>
		             <input type="email" name="email" class="input" value="{{ old('email') }}">
		  		   
		          </div>
		
		          <div class="form-group">
					  <label for="fieldUser">Password</label>
		            <input type="password" class="input" name="password">
		  		  
		          </div>
		
		          <div class="form-group">
					  <label for="fieldUser" >Confirm Password</label>
		            <input type="password" class="input" name="password_confirmation">
		  		  
		          </div>
		
		           <div class="well well-sm">
		            <button type="submit" class="btn">Register</button>
		          </div>
	
		  	</form>

        </div>
    </div>
@endsection
	
	
