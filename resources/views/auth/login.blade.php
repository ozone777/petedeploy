<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
	
	<link rel="icon" type="image/png" href="/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	
	
	
	<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="manifest" href="/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	
    <title>Wordpress Pete!</title>

        <link rel="stylesheet" href="/css/style.css">
		<script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.3.min.js"></script>
  </head>

  <body>

    <!--Google Font - Work Sans-->
<link href='https://fonts.googleapis.com/css?family=Work+Sans:400,300,700' rel='stylesheet' type='text/css'>

<div class="container">
  <div class="profile">
    <button class="profile__avatar" id="toggleProfile">
     <img src="/face.png" alt="Massive Server" style="width:58px;height:70px;">
	 <p>Click Here</p>
    </button>
	
	<form method="POST" action="/auth/login">
	    {!! csrf_field() !!}
	
    <div class="profile__form">
      <div class="profile__fields">
        <div class="field">
			
          <input type="text" id="email" name="email" value="{{ old('email') }}" class="input" required pattern=.*\S.* />
          <label for="fieldUser">Email</label>
        </div>
        <div class="field">
          <input type="password" id="password" name="password" class="input" required pattern=.*\S.* />
          <label for="fieldPassword">Password</label>
        </div>
        <div class="profile__footer">
          <button class="btn">Login</button>
        </div>
      </div>
     </div>
  </div>

	</form>

</div>
    
        <script src="/js/index.js"></script>

    <script>

	$( document ).ready(function() {
		if("{{$sw_user}}" == "false"){
			window.location.replace("/auth/register");
		}
	});
    </script>
    
    
  </body>
</html>
