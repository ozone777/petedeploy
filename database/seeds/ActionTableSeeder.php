<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class ActionTableSeeder extends Seeder {

    public function run()
    {
		
		DB::table('actions')->delete();
		DB::table('actions')->insert(['id' => '1','name' => 'New', 'body' => 'Create new App', 'created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('actions')->insert(['id' => '2','name' => 'Clone', 'body' => 'Clone your App', 'created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('actions')->insert(['id' => '3','name' => 'Move', 'body' => 'Move your App To Another URL', 'created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('actions')->insert(['id' => '4','name' => 'Import', 'body' => 'Import your App', 'created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('actions')->insert(['id' => '5','name' => 'Export', 'body' => 'Export your App', 'created_at' => new DateTime, 'updated_at' => new DateTime]);
		
		DB::table('apps')->delete();
		DB::table('apps')->insert(['id' => '1','name' => 'Wordpress', 'body' => 'Wordpress CMS', 'created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('apps')->insert(['id' => '2','name' => 'Drupal', 'body' => 'Drupal CMS', 'created_at' => new DateTime, 'updated_at' => new DateTime]);
		
		DB::table('apps_actions')->delete();
		DB::table('apps_actions')->insert(['id' => '1','app_id' => '1', 'action_id' => '1', 'script' => 'test', 'created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('apps_actions')->insert(['id' => '2','app_id' => '1', 'action_id' => '2', 'script' => 'test', 'created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('apps_actions')->insert(['id' => '3','app_id' => '1', 'action_id' => '3', 'script' => 'test', 'created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('apps_actions')->insert(['id' => '4','app_id' => '1', 'action_id' => '4', 'script' => 'test', 'created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('apps_actions')->insert(['id' => '5','app_id' => '1', 'action_id' => '5', 'script' => 'test', 'created_at' => new DateTime, 'updated_at' => new DateTime]);
		
		DB::table('apps_actions')->insert(['id' => '6','app_id' => '2', 'action_id' => '1', 'script' => 'test', 'created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('apps_actions')->insert(['id' => '7','app_id' => '2', 'action_id' => '2', 'script' => 'test', 'created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('apps_actions')->insert(['id' => '8','app_id' => '2', 'action_id' => '3', 'script' => 'test', 'created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('apps_actions')->insert(['id' => '9','app_id' => '2', 'action_id' => '4', 'script' => 'test', 'created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('apps_actions')->insert(['id' => '10','app_id' => '2', 'action_id' => '5', 'script' => 'test', 'created_at' => new DateTime, 'updated_at' => new DateTime]);
		
		
		DB::table('options')->delete();
		DB::table('options')->insert(['id' => '1','option_name' => 'db_root_password', 'option_value' =>'root','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('options')->insert(['id' => '2','option_name' => 'app_root', 'option_value' => '/Applications/MAMP/htdocs/laravel/','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('options')->insert(['id' => '3','option_name' => 'apache_conf', 'option_value' => '/Applications/MAMP/conf/apache/extra/httpd-vhosts.conf','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('options')->insert(['id' => '4','option_name' => 'mysql_bin', 'option_value' => '/Applications/MAMP/Library/bin/','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('options')->insert(['id' => '5','option_name' => 'os', 'option_value' => 'osx','created_at' => new DateTime, 'updated_at' => new DateTime]);
		
		
    }

}