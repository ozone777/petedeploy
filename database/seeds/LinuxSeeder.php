<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;


class LinuxSeeder extends Seeder {

    public function run()
    {
		
		DB::table('options')->delete();
		
		DB::table('options')->insert(['id' => '2','option_name' => 'app_root', 'option_value' => '/var/www','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('options')->insert(['id' => '3','option_name' => 'server_conf', 'option_value' => '/etc/httpd/conf.d','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('options')->insert(['id' => '4','option_name' => 'mysql_bin', 'option_value' => '','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('options')->insert(['id' => '5','option_name' => 'os', 'option_value' => 'linux','created_at' => new DateTime, 'updated_at' => new DateTime]);
		
	    
    }

}